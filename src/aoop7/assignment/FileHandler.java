    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;

/**
 *
 * @author Paul
 * Commented by Sarah
 */

import java.util.*;
import java.io.*;

/**
* Read from files and place contents into a list of characters
* Write to files from a list of characters 
**/
public class FileHandler{
    /*
    static void readFlie(String in) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/

    //Variables
    FileReader inFile;
    FileWriter outFile;
    
    //Constructor
    public FileHandler(){
        
    }
    /**
    * readFile method takes a given filename and attempts to read the file.
    * Each character in the file is put into a list
    * The file will then be closed.
    * If there is an issue, such as the file does not exist, an error will be thrown.
    * 
    * @param fileName The file the user wants to read
    * @return the list of characters that was in the text file
     * @throws java.io.IOException
    */ 
    public List readFile(String fileName)
    throws IOException{
        List chars = new ArrayList();
        //When reading file, must be in a Try block

        FileReader inFile = new FileReader(fileName);

        int ch = inFile.read();
        while(ch != -1) {
            chars.add(new Character((char)ch));
            ch = inFile.read();
        }
        inFile.close();

    return chars;
    }

    /**
    * writeFile method takes a given filename and attempts to write to the file.
    * Each character in a given list is written to a textFile
    * The file will then be closed.
    * 
    * @param fileName The file the user wants to read
     * @throws java.io.IOException
    */ 
    public void writeFile(String fileName, List chars)
    throws IOException{
        //When creating a new instance of a FileWriter, but be in try block
                    
        outFile = new FileWriter(fileName);
        /*Read through a list of characater, until the ned has been reached,
         Write each char in the list to a file*/             
        for (Iterator it = chars.iterator(); it.hasNext(); ) {
            char aChar = ((Character)it.next()).charValue();
            outFile.write(aChar);
        }
       outFile.close();
        
    }
}