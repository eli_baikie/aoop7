/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Sarah
 */
class FileConverter{
    //Variables
    private SimpleEncoder encoder;
    private SimpleDecoder decoder;
    private Compressor compressor;
    private FileHandler fileHandler;
    public Boolean overwritePermission; 
    //Constructor
    public FileConverter(){
        overwritePermission = false;
        encoder = new SimpleEncoder();
        decoder = new SimpleDecoder();  
        compressor = new Compressor();
        fileHandler = new FileHandler();
    }   
    public void setOverwritePermission(Boolean anAnswer){
        overwritePermission = anAnswer;
    }
    //Methods
    /**
     * Remove the file extension if it has one, then append the correct one?
     * eg. encode "sarah.txt" --> "sarah.zap" not... "sarah.txt" --> "sarah.txt.czp"
     * @param fileName the file name the user provided
     * @param expectedSuffix the suffix that is to be expected with a particular action
     * @return the correct file name to save the new contents to 
     */
    private String updateFileName(String fileName, String expectedSuffix)
    /*throws fileOverwriteException*/{
        if (fileName.length() > 4){
            String suffix = fileName.substring((int)fileName.length()-4, fileName.length());
            if(suffix.equals(".txt") || suffix.equals(".zap") || suffix.equals(".czp")){
                return fileName.substring(0, fileName.length()-4) + expectedSuffix;              
            }
        }
        //If the filename does not contain a suffix add it
        return fileName + expectedSuffix;
    }
    /**
    * Encode the specified file into 'zap' format. The file must exist, and
    * it must be a text file with suffix ".txt". The filename may or may not
    * include the suffix. If not, the ".txt" suffix is automatically appended.
    * The method creates an encoded file with the same base name and ".zap"
    * suffix.
    *
    * @param filename The file name of the file to be encoded.
    * */
    public void encode(String orginalFile)           
    throws FileOverwriteException, FileDoesNotExistException, IOException{
        String fileName = updateFileName(orginalFile, ".txt");
        File aFile = new File(fileName); 
        if(!aFile.exists() ){   
            throw new FileDoesNotExistException(fileName);            
        }
        List fileContents = fileHandler.readFile(orginalFile);
        String encodedFile = updateFileName(fileName, ".zap"); 
        List encodedList = encoder.encode(fileContents);
        
        aFile = new File(encodedFile);    
        /* If a file exists,  */
        if (!aFile.exists() || overwritePermission){
            fileHandler.writeFile(encodedFile, encodedList);
        } else {
            FileOverwriteException newE = new FileOverwriteException(fileName);
            throw newE;        
        }
    }
    /**
    * Decode the specified file from 'zap' to 'txt' format. The file must
    * exist, and it must be a zap-encoded file with suffix ".zap". The
    * filename may or may not include the suffix. If not, the ".zap" suffix
    * is automatically appended. The method creates a decoded file with
    * the same base name and ".txt" suffix.
    *
    * @param filename The file name of the file to be decoded.
    */
    public void decode(String orginalFile)
    throws InvalidFormatException, EmptyFileException, FileOverwriteException, IOException {
        //Update the file name so that is has correct suffix
        String fileName = updateFileName(orginalFile, ".zap");
        //Check if the file exists
        File aFile = new File(fileName);/*
        if(aFile){
            InvalidFormatException newE = new InvalidFormatException("decode", orginalFile);
            throw newE;
        }*/
        if(!aFile.exists() ){   
            throw new FileDoesNotExistException(fileName);            
        }
        List fileContents = fileHandler.readFile(fileName);

        String decodedFile = updateFileName(fileName, ".txt");
        try{
            List decodedList = decoder.decode(fileContents);
            //Before writing to the new file, ensure that you are not going to overwrite someone eles's work
            aFile = new File(decodedFile);    
            if (!aFile.exists() || overwritePermission){
                fileHandler.writeFile(decodedFile, decodedList);
            } else {
                FileOverwriteException newE = new FileOverwriteException(fileName);            
                throw newE;        
            }
        }
        catch (IndexOutOfBoundsException e){
            //If the file is in the wrong format, and you cant decode it
            InvalidFormatException newE = new InvalidFormatException(fileName); /* "decode", updateFileName(fileName, ".zap")*/
            throw newE;
        }   
    }

    /**
    * Compress the specified file into 'czp' format. The file must exist, and
    * it must be a text file with suffix ".txt". The filename may or may not
    * include the suffix. If not, the ".txt" suffix is automatically appended.
    * The method creates an encoded file with the same base name and ".czp"
    * suffix.
    *
    * @param filename The file name of the file to be compressed.
    */
    public void compress(String orginalFile) 
    throws FileOverwriteException, EmptyFileException, InvalidFormatException, IOException{
        String fileName = updateFileName(orginalFile, ".txt");
        List fileContents = fileHandler.readFile(fileName);
        if (fileContents.size() < 1){
            try{
                throw new EmptyFileException("File Empty");
            }
            catch(EmptyFileException e){
                throw e;
            }
        }
        else{
            String compressedFile = updateFileName(fileName, ".czp"); 
            try{
                List compressedList = compressor.compress(fileContents);
                File aFile = new File(compressedFile);    
                if (!aFile.exists() || overwritePermission){
                    fileHandler.writeFile(compressedFile, compressedList);
                } else {
                    FileOverwriteException newE = new FileOverwriteException(fileName);            
                    throw newE;        
                }
            }
            catch (IndexOutOfBoundsException e){
                InvalidFormatException newE = new InvalidFormatException("compress", updateFileName(fileName, ".czp"));      
                System.out.println(newE.errorMessage()); 
            } 
        }
    }

    /**
    * Uncompress the specified file from 'czp' to 'txt' format. The file must
    * exist, and it must be a czp-encoded file with suffix ".czp". The
    * filename may or may not include the suffix. If not, the ".czp" suffix
    * is automatically appended. The method creates an uncompressed file with
    * the same base name and ".txt" suffix.
    *
    * @param filename The file name of the file to be uncompressed.
    */
    public void uncompress(String orginalFile)
    throws FileOverwriteException, EmptyFileException, InvalidFormatException, IOException{
        /*
        String orginalFile = updateFileName(fileName, ".czp");
        List fileContents = fileHandler.readFile(orginalFile);
        
        String uncompressedFile = updateFileName(fileName, ".txt"); 
        List uncompressedList = compressor.uncompress(fileContents);
        fileHandler.writeFile(uncompressedFile, uncompressedList);
        */
        String fileName = updateFileName(orginalFile, ".czp");
        List fileContents = fileHandler.readFile(orginalFile);
        if (fileContents.size() < 1){
            try{
                throw new EmptyFileException("File Empty");
            }
            catch(EmptyFileException e){
                throw e;
            }
        }
        else{
            String uncompressedFile = updateFileName(fileName, ".txt"); 
            try{
                List uncompressedList = compressor.uncompress(fileContents);
                File aFile = new File(uncompressedFile);    
                if (!aFile.exists() || overwritePermission){
                    fileHandler.writeFile(uncompressedFile, uncompressedList);
                } else {
                    FileOverwriteException newE = new FileOverwriteException(fileName);            
                    throw newE;        
                }                
            }
            catch (IndexOutOfBoundsException e){
                InvalidFormatException newE = new InvalidFormatException("uncompress", updateFileName(fileName, ".txt"));              
                System.out.println(newE.errorMessage()); 
            } 
        }
    }

    /**
    * Show the content of the requested file in the text terminal.
    * The file must exist, and it must be a text file with suffix ".txt".
    * The filename may or may not include the suffix. If not, the ".txt"
    * suffix is automatically appended.
    *
    * @param filename The file name of the file to be displayed.
    */
    public void showFile(String orginalFile)
    throws IOException{
        String fileName = updateFileName(orginalFile, ".txt");
        List<Character> fileContents = fileHandler.readFile(fileName);
        
        for (Character aChar : fileContents){
            System.out.print(aChar.charValue());
        }
        
    }

    /**
    * Decode and show the content of the requested file in the text terminal.
    * The file must exist, and it must be a text file with suffix ".zap".
    * The filename may or may not include the suffix. If not, the ".zap"
    * suffix is automatically appended. The file is decoded and displayed.
    *
    * @param filename The file name of the file to be displayed.
    */
    public void decodeAndShowFile(String fileName)
    throws InvalidFormatException, EmptyFileException, FileOverwriteException, IOException{
        try{
            decode(fileName);
            showFile(fileName);
        }
        catch (InvalidFormatException e){
            throw e;
        }
        catch(EmptyFileException e){
            throw e;
        }
        catch(FileOverwriteException e){
            throw e;
        }
        catch(IOException e){
            throw e;
        }        
    }
}
