/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;



/**
 *
 * @author Eli
 */
class SimpleDecoder{
    
    
    //Variables
    private List<Character> alphabet;
    private int alphabetLength;
    
    
    //Constructor
    public SimpleDecoder(){
        //Empty
    }
    

    //Methods
    /* Create the alphabet by looking the first charcater, and converting it to an int. 
        This will determine the length of the alphabet. 
        Now knowing how long the alphabet should be, add the next characters in the list to alphabet. */
    private void createAlphabet(List aList)
    throws IndexOutOfBoundsException{
        //read a Character and then save it and make it a number
        
        alphabet = new ArrayList<>();
        
        alphabetLength = ((int)((Character)aList.get(0))); /*Convert initial char to int*/

        ///for the rest of the list do stuff but only for the length of the alphabet
        
        for(int index = 1; index < alphabetLength + 1; index ++){
            alphabet.add((Character)aList.get(index));
        }
    }
    
    
    public List decode(List encoded)
        throws IndexOutOfBoundsException{
            
        
        List<Character> decoded = new ArrayList<>();

        createAlphabet(encoded);


        
        /* For each next char to the end of the given List,
        - Read char
        -convert to int
        -lookup alphabet.get(index)
        -add alphabet.get(index) to "decoded" List
        -return "decoded" List
        */
        for(int index = (alphabetLength + 1); index < encoded.size(); index ++){   
            decoded.add(alphabet.get((int)(Character)encoded.get(index)));     
        }
        return decoded;
    }
            
}
