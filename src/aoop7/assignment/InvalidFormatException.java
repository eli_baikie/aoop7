/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;

import java.io.IOException;

/**
 *
 * @author Sarah
 */
public class InvalidFormatException
        extends IOException{
    private String errorMessage;
    /**
     * 
     * @param fileName The file name that is not in correct Format
     */
    public InvalidFormatException(String fileName){
        errorMessage = fileName + " may be corrupt";
    }
    /* A user may try to decode a .czp, which will not work because it is in the wrong format*/
     public InvalidFormatException(String action, String fileName){
        errorMessage = errorMessage = "Cannot " + action + " " + fileName + " because it is in the wrong format";
    }

    public String errorMessage(){
        return errorMessage;
    }
}