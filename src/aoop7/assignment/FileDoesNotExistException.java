/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;

import java.io.IOException;

/**
 *
 * @author Sarah
 */
public class FileDoesNotExistException  extends IOException{
    private String fileName;
    /**
     * 
     * @param fileName The file name that does not
     */
     public FileDoesNotExistException(String fileName){
        this.fileName = fileName;
    }
    public String errorMessage(){
        return "The file; " + fileName + " does not exist";
    }
    
}
