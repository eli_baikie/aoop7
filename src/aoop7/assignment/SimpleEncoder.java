/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Sarah
 */
class SimpleEncoder{  
    //Variables
    private List<Character> alphabet;
    private List<Character> encodedList;
    //Constructor
    public SimpleEncoder(){
        encodedList = new ArrayList<>();
    }
    //Methods
    
    /* Remove duplicated from a list using a HashSet*/
    private List removeDuplicates(List aList){
        HashSet<Character> duplicateFree = new HashSet<>(aList);
        return new ArrayList<>(duplicateFree);
    }
    /* Find the ASCII code that represents that character*/
    private char toAscii(Character aChar){
        int alphabetIndex = findIndex(aChar);
        return (char) alphabetIndex; //(int) findIndex(aChar);
    }
    /* Returns the index of a character in the alphabet*/
    private int findIndex(Character aChar){
        return alphabet.indexOf(aChar);
    }
    
    /*  Removes all duplicates from a given list, and creates an alphabet
        Calculate how long the alphabet is, and add this is the first ASCII value
        Write the Character with the ASCII Code int of the index it is in the alpahbet
        */
    public List encode(List<Character> aList){
        //Create the alphabet
        alphabet = removeDuplicates(aList);

        //Add the ASCII character that represents the length of the alphabet
        encodedList.add((char)alphabet.size());
        //Add alphabet directly into encoded list
        for (Character aChar : alphabet )
        {
            encodedList.add(aChar);
        }
        //for each character in the list, find its position in the alphabet, convert that index to ASCII, add to encodedList
        for (Character aChar : aList)
        { 
            encodedList.add(toAscii(aChar));
        } 
        return encodedList;
    }
}