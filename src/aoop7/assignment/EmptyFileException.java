/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;

//import java.io.IOException;

/**
 *
 * @author ebaik
 */
public class EmptyFileException extends Exception {

    /**
     * Creates a new instance of <code>EmptyFileException</code> without detail
     * message.
     */
    public EmptyFileException() {
        
          
    }
    public String errorMessage(){
    return "Given file is contains no valid data or does not exist";
    } 
    
    /**
     * Constructs an instance of <code>EmptyFileException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public EmptyFileException(String msg) {
        super(msg);
    }
}
