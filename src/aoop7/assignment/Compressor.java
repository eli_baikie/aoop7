    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aoop7.assignment;
 
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eli
 */
class Compressor{
    //Variables
    
    //Construtor
    public Compressor(){
        
    }
    //Methods
    public List compress(List<Character> text){ 
        List<Character> compressed = new ArrayList<>();
        
        Character previous = text.get(0);
        int count = 1;

        for(int index = 1; index < text.size(); index ++){       
            if(previous.equals(text.get(index))){
                count = count + 1;
            }
            else{
                //Write the orginal character and the amount of occurances of that character (In ASCII not decimal)
                compressed.add(previous);
                compressed.add((char) count);
                // Start looking at the next character and reset the counter
                previous = text.get(index);
                count = 1;                
            }
        }
        compressed.add(previous);
        compressed.add((char) count);
        return compressed;
    }
    public List uncompress(List text){
        List<Character> decompressed = new ArrayList<>();
        Character character;
        int multiplicity;  
        for (int index = 0; index < text.size(); index += 2 ){ 
            character = (Character)text.get(index);
            multiplicity = (int)(Character)text.get(index + 1);

            for (int writer = 0; writer < multiplicity; writer++){
                
                decompressed.add(character);
            }
        }
        return decompressed;
        
    }
            
}
