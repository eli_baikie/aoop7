
package aoop7.assignment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner; //User input

/**
 *
 * @author Eli/Sarah
 */
public class AssignmentAOOP7 {

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) 
    throws IOException {
        FileConverter fileConverter = new FileConverter();
        Scanner userInput;
        String in;
        Boolean processFinished;
        boolean exit = false;
        String introduction = (
            "\n"
            + "AOOP7 Simple encoder, decoder, compressor \n"
            + "ENCODE-          CONVERT A PLAIN TEXT FILE WITH THE FILE EXTENSION .TXT TO A .ZAP FILE \n"
            + "DECODE-          CONVERT AN ENCODED .ZAP FILE TO A PLAIN TEXT FILE WITH THE FILE EXTENSION .TXT \n"
            + "DECODE.S-        CONVERT AN ENCODED .ZAP FILE TO A PLAIN TEXT FILE WITH THE FILE EXTENSION .TXT AND DISPLAY FILE ON SCREEN \n"
            + "COMPRESS-        CONVERT A .ZAP FILE TO A COMPRESSED .CZP FILE \n"
            + "UNCOMPRESS-      CONVERT A COMPRESSED .CZP FILE TO A .ZAP FILE \n"
            + "SHOWFILE-        SHOW FILE ON SCREEN"
            + "HELP-            HELP \n"
            + "EXIT-            EXIT"
        );       

        System.out.println(introduction);

        while (exit == false){
    
            userInput = new Scanner(System.in);
            in = userInput.nextLine();
            processFinished = false;
            
            switch(in.toLowerCase()){
                 case "encode" :
                    fileConverter.setOverwritePermission(false);
                    System.out.println("what file would you like to encode?");

                    in = userInput.nextLine();
                    processFinished = false;
                    while (!processFinished)
                    {
                        try{
                            /* See if a file already exists*/
                            fileConverter.encode(in);
                            processFinished = true;
                        } 
                        catch(FileDoesNotExistException e){
                            System.out.println(e.errorMessage());
                            processFinished = true;
                        }
                        /*
                        catch(EmptyFileException e){
                            System.out.println("An error occured: " + e.errorMessage());
                            processFinished = true;
                        }*/
                        catch(FileOverwriteException e){
                            /* If it does exist, ask the user if we can overwrite it*/
                            System.out.println(e.errorMessage());
                            System.out.println("Would you like to overwrite this file? (Y/N)");
                            e.overwrite(userInput.nextLine());                  
                            fileConverter.setOverwritePermission(e.getOverwritePermission());
                            /* If the user give permisson, we want the file encoded to be not yet.
                            If the user doesnt want to overwrite, we have finihsed... and the file has been dealt with*/
                            processFinished = !e.getOverwritePermission();
                        }catch(IOException e){
                            System.out.println("Unexpected error reading file"); 
                            processFinished = true;
                        }
                    }
                    break;
                 case "decode" :
                    fileConverter.setOverwritePermission(false);
                    System.out.println("What file would you like to decode?");
                    in = userInput.nextLine();
                    processFinished = false;
                    while (!processFinished)
                    {
                        try{
                            /* See if a file already exists*/
                            fileConverter.decode(in);
                            processFinished = true;
                        }
                        
                        catch(EmptyFileException e){
                            System.out.println("An error occured: " + e.errorMessage());
                            processFinished = true;
                        }
                        catch(FileOverwriteException e){
                            /* If it does exist, ask the user if we can overwrite it*/
                            System.out.println(e.errorMessage());
                            System.out.println("Would you like to overwrite this file? (Y/N)");
                            e.overwrite(userInput.nextLine());                  
                            fileConverter.setOverwritePermission(e.getOverwritePermission());
                            /* If the user give permisson, we want the file encoded to be not yet.
                            If the user doesnt want to overwrite, we have finihsed... and the file has been dealt with*/
                            processFinished = !e.getOverwritePermission();
                        }catch(IOException e){
                            System.out.println("Unexpected error reading file"); 
                            processFinished = true;
                        }
                    }                      
                    break;
                 case "compress" :               
                    fileConverter.setOverwritePermission(false);
                    System.out.println("What file would you like to compress?");
                    in = userInput.nextLine();
                    processFinished = false;
                    while (!processFinished)
                    {
                        try{

                            fileConverter.compress(in);
                        }
                        catch(EmptyFileException e){
                            System.out.println("An error occured: " + e.errorMessage());
                            processFinished = true;
                        }
                        catch(FileOverwriteException e){
                            /* If it does exist, ask the user if we can overwrite it*/
                            System.out.println(e.errorMessage());
                            System.out.println("Would you like to overwrite this file? (Y/N)");
                            e.overwrite(userInput.nextLine());                  
                            fileConverter.setOverwritePermission(e.getOverwritePermission());
                            /* If the user give permisson, we want the file encoded to be not yet.
                            If the user doesnt want to overwrite, we have finihsed... and the file has been dealt with*/
                            processFinished = !e.getOverwritePermission();
                        }catch(IOException e){
                            System.out.println("Unexpected error reading file");
                            processFinished = true;
                        }
                    }                    
                    break;
                 case "uncompress" :
                    fileConverter.setOverwritePermission(false);
                    System.out.println("What file would you like to uncompress?");
                    in = userInput.nextLine();
                    processFinished = false;
                    while (!processFinished)
                    {
                        try{
                            fileConverter.uncompress(in);
                        }
                        catch(EmptyFileException e){
                            System.out.println("An error occured: " + e.errorMessage());
                            processFinished = true;
                        }
                        catch(FileOverwriteException e){
                            /* If it does exist, ask the user if we can overwrite it*/
                            System.out.println(e.errorMessage());
                            System.out.println("Would you like to overwrite this file? (Y/N)");
                            e.overwrite(userInput.nextLine());                  
                            fileConverter.setOverwritePermission(e.getOverwritePermission());
                            /* If the user give permisson, we want the file encoded to be not yet.
                            If the user doesnt want to overwrite, we have finihsed... and the file has been dealt with*/
                            processFinished = !e.getOverwritePermission();
                        }
                        catch(IOException e){
                            System.out.println("Unexpected error reading file"); 
                            processFinished = true;
                        }
                    }               
                    break;
                case "help" :
                    System.out.println(introduction);                
                    break;

                case "showfile" :
                    fileConverter.setOverwritePermission(false);
                    System.out.println("What file would you like to show?");
                    in = userInput.nextLine();
                    processFinished = false;
                    while (!processFinished)
                    {
                        try{
                            fileConverter.uncompress(in);
                        }
                        catch(EmptyFileException e){
                            System.out.println("An error occured: " + e.errorMessage());
                            processFinished = true;
                        }
                        catch(FileOverwriteException e){
                            /* If it does exist, ask the user if we can overwrite it*/
                            System.out.println(e.errorMessage());
                            System.out.println("Would you like to overwrite this file? (Y/N)");
                            e.overwrite(userInput.nextLine());                  
                            fileConverter.setOverwritePermission(e.getOverwritePermission());
                            /* If the user give permisson, we want the file encoded to be not yet.
                            If the user doesnt want to overwrite, we have finihsed... and the file has been dealt with*/
                            processFinished = !e.getOverwritePermission();
                        }catch(IOException e){
                            System.out.println("Unexpected error reading file"); 
                            processFinished = true;
                        }
                    }      
                    fileConverter.showFile(in);

                    break;

                case "decode.s" :      
                    fileConverter.setOverwritePermission(false);
                    System.out.println("What file would you like to decode and show?");

                    in = userInput.nextLine();
                    while (!processFinished)
                    {
                        try{
                            fileConverter.decodeAndShowFile(in);
                        }                     
                        catch(EmptyFileException e){
                            System.out.println("An error occured: " + e.errorMessage());
                            processFinished = true;
                        }
                        catch(FileOverwriteException e){
                            /* If it does exist, ask the user if we can overwrite it*/
                            System.out.println(e.errorMessage());
                            System.out.println("Would you like to overwrite this file? (Y/N)");
                            e.overwrite(userInput.nextLine());                  
                            fileConverter.setOverwritePermission(e.getOverwritePermission());
                            /* If the user give permisson, we want the file encoded to be not yet.
                            If the user doesnt want to overwrite, we have finihsed... and the file has been dealt with*/
                        }catch(IOException e){
                            System.out.println("Unexpected error reading file"); 
                            processFinished = true;
                        }
                    }

                    break;

                case "exit" :
                    System.out.println("Goodbye!");
                    exit = true;

                    break;
                default :
                    System.out.println(in + " is not a valid input.");
                    break;
                }
            if (!exit)
            {
              System.out.println("\n" + "What now?");
            }
        }
    }
}
